<?php
/**
 * Plugin Name: Hellio Messaging
 * Plugin URI: https://helliomessaging.com/
 * Description: A powerful SMS Messaging/Texting plugin for WordPress
 * Version: 5.3.1
 * Author: Hellio Messaging
 * Author URI: https://helliomessaging.com/
 * Text Domain: hellio-sms
 * Domain Path: /languages
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * Load Plugin Defines
 */
require_once 'includes/defines.php';

/**
 * Load plugin Special Functions
 */
require_once WP_SMS_DIR . 'includes/functions.php';

/**
 * Get plugin options
 */
$wpsms_option = get_option( 'wpsms_settings' );

/**
 * Initial gateway
 */
require_once WP_SMS_DIR . 'includes/class-wpsms-gateway.php';

$sms = wp_sms_initial_gateway();

/**
 * Load Plugin
 */
require WP_SMS_DIR . 'includes/class-wpsms.php';

new WP_SMS();
